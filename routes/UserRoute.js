const express = require('express');
const router =express.Router();
const User  = require('../models/User');
var jwt = require('jsonwebtoken');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');

//login
router.post('/login',async (req, res) =>
{
  try{

    const NewUser =await User.find({ email : req.body.email  }).limit(1);
    const decryptedString = cryptr.decrypt(NewUser[0].password);
    if (NewUser.length < 1)
    {
      await res.json({status: "err", message: 'Email Does not Exists'});
      return ;
    }
    if (decryptedString !== req.body.password )
    {
      await res.json({status:"err" , message: 'Wrong Paswword'});

      return ;
    }
    if (NewUser[0].enabled === 0 )
    {
      await res.json({status:"err" , message: 'User is Disabled'});
      return ;
    }
    var payload = {
      id: NewUser[0]._id,
    } ;
    let token = jwt.sign(payload,'tawfik');
      res.json({status:"ok" , message: 'Welcome Back', UserData : NewUser , token});
  }catch (err) {
    res.header("Access-Control-Allow-Headers", "*");
    res.json({ message:err.message });
  }

});

// register


router.post('/register',async (req,res) =>
{
 
  const encryptedPWD = cryptr.encrypt(req.body.password);
  let user=new User({
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        address : req.body.address,
        gender : req.body.gender,
        numTel : req.body.numTel,
        num1 : req.body.num1,
        num2 : req.body.num2,
        num3 : req.body.num3,
        password :encryptedPWD,
  });
  try{
    const NewUser =await User.find({ email : req.body.email });
    if (NewUser === undefined || NewUser.length == 0 )
    {
      user  = await user.save();
      res.json({status:"Success" , message: 'Account Create ! You can Login now'});
      return ;
    }

    res.json({status:"err" , message: 'Email Already Exists'});
  }catch (err) {
    res.header("Access-Control-Allow-Headers", "*");
    res.json({ message:err.message });
  }

}) ;

  module.exports=router;
