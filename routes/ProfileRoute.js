const express = require('express');
const router = express.Router();
const U = require('../models/User');
var jwt = require('jsonwebtoken');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');


router.post('/update', verifyToken, async (req, res) => {

    const us = await U.findById({_id: req.id});

    if (req.body.firstName != null) {
        us.firstName = req.body.firstName ;
    }if (req.body.lastname != null) {
        us.lastname = req.body.lastname ;
    }
    if (req.body.numTel != null) {
        us.numTel = req.body.numTel ;
    }
    if (req.body.password != null) {
        const encryptedString = cryptr.encrypt(req.body.password);
        us.password = encryptedString ;
    }
    if (req.body.email != null) {
        us.email = req.body.email;

    }
    us.save() ;

    await res.json(us);
});

module.exports = router;
